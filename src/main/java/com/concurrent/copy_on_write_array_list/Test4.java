package com.concurrent.copy_on_write_array_list;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class Test4 {

    //private List<Integer> list = new ArrayList<>();
    private List<Integer> list = new CopyOnWriteArrayList<>();

    public Test4() {
        addElementsToList();
    }

    public static void main(String[] args) throws Exception {

        Test4 test4 = new Test4();

        Runnable task1 = () -> {
            test4.list.add(10);
        };

        Iterator<Integer> iterator = test4.list.iterator();
        //iterator.next();

        //iterator.remove();//запрещенна

        test4.list.set(0, 99);

        Thread thread = new Thread(task1);
        thread.start();

        while (iterator.hasNext()) {
            System.out.print(iterator.next() + " ");
        }

        System.out.println();

        for (Integer i : test4.list) {
            System.out.print(i + " ");
        }
    }

    private void addElementsToList() {
        for (int i = 0; i < 5; i++) {
            list.add(i);
        }
    }

}
