package com.concurrent.copy_on_write_array_list;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class Test2 {
    public static void main(String[] args) {
        List<String> list = new CopyOnWriteArrayList();
        list.add("1");
        list.add("2");
        list.add("3");
        list.add("4");
        list.add("5");

        // get the iterator
        Iterator<String> it = list.iterator();
        Iterator<String> it2 = list.iterator();

        System.out.println(list);
        System.out.println("______________________");
        //manipulate list while iterating
        while(it.hasNext()){
            System.out.println("list is:"+list);
            String str = it.next();
            System.out.println(str);
            if(str.equals("1")) {
                list.set(2, "10");
                list.add("7");
                list.add("8");
                list.add("9");
            }

            if(str.equals("2"))
                list.remove("5");

            if(str.equals("3"))
                list.add("3 found");

            //below code don't throw ConcurrentModificationException
            //because it doesn't change modCount variable of list
            if(str.equals("4"))
                list.set(1, "4");

            if (str.equals("5"))
                list.add("6");
        }

        //it.remove();//Exception

        System.out.println("______________________");
        System.out.println(list);

        /*System.out.println("______________________");

        it.forEachRemaining(System.out :: print);*/

        System.out.println("______________________");

        it2.forEachRemaining(System.out :: print);
    }
}
