package com.concurrent.hashmap;

import java.util.HashMap;
import java.util.Map;

public class HashMapDemo extends Thread {
    private static Map<Integer, String> map = new HashMap<>();

    @Override
    public void run() {
        try {
            Thread.sleep(1000);
            map.put(103, "D");
        } catch (InterruptedException e) {
            System.out.println("Child Thread going to add element");
        }
    }

    public static void main(String[] args) throws InterruptedException {
        map.put(100, "A");
        map.put(101, "B");
        map.put(102, "C");
        HashMapDemo t = new HashMapDemo();
        t.start();

        for (Object object : map.entrySet()) {
            System.out.println(object);
            Thread.sleep(1000);
        }
        System.out.println(map);
    }
}
