package com.concurrent.hashmap;

import java.util.Iterator;
import java.util.Map;

public class MapIterator {
    public void iterateMapAndAdd(Map<String, String> map, String newKey, String value, String compareKey) {
        System.out.print("Iterate map:");
        Iterator<String> iter = map.keySet().iterator();

        while (iter.hasNext()) {
            String key = iter.next();
            if (key.equals(compareKey))
                map.put(newKey, value);
            System.out.print("  " + key + "=" + map.get(key));
        }
        System.out.println();
    }
}
