package com.concurrent.hashmap;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentMapDemo extends Thread {
    private static Map<Integer, String> map = new ConcurrentHashMap<>();

    public void run() {
        map.put(103, "D");

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            System.out.println("Child Thread going to add element");
        }
    }

    public static void main(String[] args) throws InterruptedException {
        map.put(100, "A");
        map.put(101, "B");
        map.put(102, "C");
        ConcurrentMapDemo t = new ConcurrentMapDemo();
        t.start();

        for (Object object : map.entrySet()) {
            System.out.println(object);
            Thread.sleep(1000);
        }
        System.out.println(map);
    }
}
