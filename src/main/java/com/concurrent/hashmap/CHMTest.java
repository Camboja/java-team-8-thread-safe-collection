package com.concurrent.hashmap;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class CHMTest {
    private Map<String, Integer> map = new ConcurrentHashMap<>();
//    private Map<String, Integer> map = new HashMap<>();

    public CHMTest() {
        initMap();
    }
    
    public static void main(String[] args) throws InterruptedException {
        CHMTest test = new CHMTest();

        Runnable task1 = () -> {
            for (Object object : test.map.entrySet()) {
                System.out.println("Print " + object);
                try {
                    Thread.sleep(300);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            };
        };

        Runnable task2 = () -> {
            for (String key : test.map.keySet()) {
                System.out.println("Remove " + test.map.remove(key));
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            };
        };

        Thread t = new Thread(task1);
        Thread t1 = new Thread(task2);


        t.start();
        t1.start();

        t.join();
        t1.join();

        System.out.println("Map after remove elements");
        System.out.println(test.map);

    }
    
    private void initMap() {
        for (int i = 0; i < 10; i++) {
            map.put("A" + i, i);
        }
    }
}
