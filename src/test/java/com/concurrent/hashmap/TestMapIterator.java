package com.concurrent.hashmap;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static org.junit.jupiter.api.Assertions.*;

class TestMapIterator {
    private MapIterator mapIterator;
    private Map<String, String> hashMap;
    private Map<String, String> concurrentMap;

    private String newKey, value, compareKey;

    @BeforeEach
    void init() {
        mapIterator = new MapIterator();
        hashMap = new HashMap<>();
        concurrentMap = new ConcurrentHashMap<>();

        hashMap.put("1", "a");
        concurrentMap.put("1", "a");
        hashMap.put("2", "b");
        concurrentMap.put("2", "b");
        hashMap.put("3", "c");
        concurrentMap.put("3", "c");
        hashMap.put("4", "d");
        concurrentMap.put("4", "d");
    }

    @Test
    @DisplayName("Modification HashMap during iteration(expected ConcurrentModificationException)")
    void modificationHashMap() {
        newKey = "5";
        value = "1";
        compareKey = "2";
        mapIterator.iterateMapAndAdd(hashMap, newKey, value, compareKey);
    }

    @Test
    @DisplayName("Modification HashMap during iteration(expected ConcurrentModificationException)," +
            "but not thrown")
    void modificationHashMap1() {
        newKey = "5";
        value = "1";
        compareKey = "4";

        printMap(hashMap);
        mapIterator.iterateMapAndAdd(hashMap, newKey, value, compareKey);
        printMap(hashMap);
    }

    @Test
    @DisplayName("Modification ConcurrentHashMap during iteration")
    void modificationConcHashMap() {
        newKey = "5";
        value = "1";
        compareKey = "2";

        printMap(concurrentMap);
        mapIterator.iterateMapAndAdd(concurrentMap, newKey, value, compareKey);
        printMap(concurrentMap);
    }

    @Test
    @DisplayName("Modification ConcurrentHashMap during iteration")
    void modificationConcHashMap1() {
        newKey = "5";
        value = "1";
        compareKey = "4";

        printMap(concurrentMap);
        mapIterator.iterateMapAndAdd(concurrentMap, newKey, value, compareKey);
        printMap(concurrentMap);
    }

    private void printMap(Map<String, String> map) {
        System.out.println("Map: " + map);
    }
}